# Amy Qualls: 48% human according to lab tests

When I had to decide what I wanted to do for a living, I acknowledged to myself that I'm a fast writer
and a slow coder, but I wanted to keep both sides of my skillset sharp. I have a not-so-secret love for
the folks who work deep in the platform, and technical writing lets me keep up with what they're doing.
I'm frequently found in team check-ins for the
[Code Review](https://about.gitlab.com/handbook/engineering/development/dev/create/code-review/) and
[Editor Extensions](https://handbook.gitlab.com/handbook/engineering/development/dev/create/editor-extensions/)
groups in the Create stage. 

## How Amy talks and thinks

- **Personality tests**: if you like this sort of thing…
  - Myers-Briggs: [INTP](https://www.forbes.com/health/mind/intp-personality-type/)
  - Enneagram, either: [5w6](https://www.crystalknows.com/enneagram/type-5/wing-6) or 
    [6w5](https://www.crystalknows.com/enneagram/type-6/wing-5).
- **Communication style**: I have a strong preference for written communication.
  I don't process or retain auditory information well.
- **Socialization style**: 
  - At _most_, small groups of people. I can't hear well in situations with
    background noise or overlapping conversations. 
  - I tend to go silent in groups larger than about 4 people unless I'm masking in some way. 
    (See also, "Amy the Docs Fairy".)
  - I'm deeply introverted. Group gatherings are intimidating, and they drain me quickly.
    I do much better in 1:1 conversations, where I can focus on the person speaking to me.
  - I'm dreadful at social cues. Sometimes I catch them. Mostly I don't.

## How Amy works

In short:

  - Deep, solitary focus, with a strong preference for written information.
  - Easily distracted by background noises and conversation.
    I wear studio monitors almost all day, every day at work.
  - Given the choice between a recorded Zoom meeting or an agenda,
    I will read the agenda / notes 100% of the time.
  - Because I work very quickly, it is easy to miss that I don't process simultaneous inputs well,
    and get overwhelmed by them.

I work exclusively from to-do items, and I start each day by processing the MRs I've been asked to review.
If you need my attention, adding me as a reviewer is the fastest way to get it. I tend to start in the early morning,
US west coast time. In my mornings I sit in on team check-ins and do reviews; new writing and
deep edits tend to happen in my afternoons.

If I have a few minutes between tasks, I'll frequently find something small to clean up. ABC: always be cleaning!

_(Taken from [this template](https://gitlab.com/gitlab-com/people-group/dib-diversity-inclusion-and-belonging/diversity-and-inclusion/-/blob/master/.gitlab/issue_templates/Team-Member-Profile.md))_

## Other notes

I love the infinite malleability of language. 

I focus on building active, positive relationships with my engineers, PMs, and designers to ensure
good ideas are captured, then turned into good contributions.

I ❤️ my command line, believe in committing directly to branches, think the internet is held
together by dirty bash scripts, and will absolutely talk your ear off about the intersection of
[documentation and testing](https://docs.gitlab.com/ee/development/documentation/testing.html).
(Ask me about Vale or Markdownlint!)
